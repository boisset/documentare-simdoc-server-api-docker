FROM debian:stretch

MAINTAINER Denis Boisset "denis.boisset@orange.com" and Christophe Maldivi "christophe.maldivi@orange.com"

RUN apt-get update && apt-get -y upgrade && apt-get install -y openjdk-8-jdk-headless bzip2 libgvc6 libopencv-core2.4v5 libopencv2.4-java imagemagick && apt-get clean

# INSTALL GRAPHVIZ
RUN mkdir -p /opt/local
WORKDIR /opt/local
COPY graphviz_bin_amd64_2.38.0.tar.bz2 /opt/local/
RUN tar xvjf graphviz_*.tar.bz2 && rm graphviz_*.tar.bz2

# TEST GRAPHVIZ
RUN /opt/local/bin/dot -V
RUN /opt/local/bin/gvmap -V

# RUN AS USER
RUN useradd -ms /bin/bash user
USER user
WORKDIR /home/user

CMD echo "Hello the documentare-simdoc-server-api-docker"

# Install our application
COPY app.jar /home/user/

EXPOSE 8080

# Start app
CMD java -jar app.jar